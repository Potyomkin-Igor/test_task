import java.util.Random;

public class Solution {
    private static final int INTEGER_TEN = 10;

    public static void main(String[] args) {
        int[] randomArray = generateRandomArray();
        int result = solution(randomArray);

        if (result == -1) {
            System.out.println("Array is empty");
        } else {
            System.out.println("Absolute sum of min abs slice: " + result);
        }
    }

    public static int solution(int[] a) {
        int minAbsSlice;
        if (a.length == 0) {
            return -1;
        }

        if (a.length == 1) {
            minAbsSlice = Math.abs(a[0]);
        } else {
            minAbsSlice = getAbsoluteSumOfMinAbsSlice(a);
        }

        return minAbsSlice;
    }

    private static int getAbsoluteSumOfMinAbsSlice(int[] arrayToCompute) {
        int result = Integer.MAX_VALUE;
        for (int i = 0; i < arrayToCompute.length; i++) {
            if (i == arrayToCompute.length - 1) {
                int lastItem = Math.abs(arrayToCompute[i]);
                if (lastItem < result) {
                    result = lastItem;
                }
                return result;
            }

            for (int j = i + 1; j < arrayToCompute.length; j++) {
                int[] subArray = getSubArray(i, j, arrayToCompute);
                int absSliceResult = computeSubArray(subArray);

                if (absSliceResult < result) {
                    result = absSliceResult;
                }
            }
        }

        return result;
    }

    private static int[] getSubArray(int beg, int end, int[] array) {
        int[] subArray = new int[end - beg + 1];
        System.arraycopy(array, beg, subArray, 0, subArray.length);
        return subArray;
    }

    private static int computeSubArray(int[] subArray) {
        int result = subArray[0];
        for (int i = 1; i < subArray.length; i++) {
            result += subArray[i];
        }

        return Math.abs(result);
    }

    private static int[] generateRandomArray() {
        Random random = new Random();
        int[] randomArray = new int[INTEGER_TEN];

        for (int i = 0; i < randomArray.length; i++) {
            randomArray[i] = random.nextInt(INTEGER_TEN + 1 + INTEGER_TEN) - INTEGER_TEN;
        }
        return randomArray;
    }
}
